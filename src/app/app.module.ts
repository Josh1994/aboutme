import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { PresentacionComponent } from './presentacion/presentacion.component';
import { HomeComponent } from './home/home.component';
import { EstudiosComponent } from './estudios/estudios.component';
import { HobbiesComponent } from './hobbies/hobbies.component';
import { AppRoutingModule } from './/app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { HttpClient } from 'selenium-webdriver/http';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    PresentacionComponent,
    HomeComponent,
    HobbiesComponent,
    EstudiosComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule,
    AppRoutingModule,
    RouterModule.forRoot([{
      path:'TEST',
      component: AppComponent
    }])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
