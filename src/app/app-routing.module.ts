import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PresentacionComponent } from './presentacion/presentacion.component';
import { HomeComponent } from './home/home.component';
import { HobbiesComponent } from './hobbies/hobbies.component';
import { EstudiosComponent } from './estudios/estudios.component';


const appRoutes: Routes = [
  { path: 'PRESENTACION', component: PresentacionComponent },
  { path: 'HOME', component: HomeComponent },
  { path: 'ESTUDIOS', component: EstudiosComponent },
  { path: 'HOBBIES', component: HobbiesComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: [],
  exports: [ RouterModule ]
})



export class AppRoutingModule {}
